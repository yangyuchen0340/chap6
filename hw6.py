import os
# choose mms 1 or mms 2 by altering the options
for i in range(1,7):
    # grid sequencing
    options_2 = [' -snes_type newtonls -da_refine 1 -snes_grid_sequence %d'%i, #-snes_stol 1e-10 ',
               '-ksp_rtol 1e-9 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin', 
               '-mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
               '-mg_levels_pc_type sor -pc_mg_type full -mms 1',
               '-snes_monitor -snes_converged_reason -ksp_converged_reason ']

    # these two options without grid sequencing
    options_0 = ['-pc_type mg -pc_mg_levels 3 -pc_mg_galerkin',
               ' -da_grid_x 17 -da_grid_y 17 -da_refine %d'%i,
               ' -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
               ' -mg_levels_pc_type sor -pc_mg_type full -mms 1 ',]    

    options_1 = [' -snes_type newtonls',
               '-da_grid_x 17 -da_grid_y 17 -da_refine %d '%i,
               '-pc_type mg -pc_mg_levels 3 -pc_mg_galerkin', 
               '-mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
               '-mg_levels_pc_type sor -pc_mg_type full -mms 1',
               '-snes_monitor -snes_converged_reason -ksp_converged_reason ']
    # preconditioned grid sequencing
    # seems fast, yet not working as accurately
    options_3 = [' -snes_type newtonls -snes_grid_sequence %d '%i,
               ' -da_refine 1 -ksp_rtol 1e-9 ',
               ' -pc_type gamg -mms 2 ',
               ' -snes_converged_reason ']
                  
    os.system('./bin/ex5 '+' '.join(options_2))
    


