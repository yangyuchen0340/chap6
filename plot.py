#!/usr/bin/env python
import numpy as np

from pylab import legend,plot,loglog,show,title,xlabel,ylabel
N = np.array([169,625,2401,9409,37209])
el2=np.array([8.31821e-05,1.12811e-05,1.51385e-06,1.97862e-07,2.53544e-08])
einf=np.array([0.00346796,0.000873184,0.00023658,7.06996e-05,2.06384e-05])
loglog(N, el2, 'r', N, 0.1*N**-1.5, 'r--',N, einf, 'g', N, 0.5/N, 'g--')
title('SNES ex5 grid sequencing MMS 1')
xlabel('Number of Dof N')
ylabel('Solution Error e')
legend(['l_2','h^{-3} = N^{-3/2}',
'l_infty', 'h^{-2} = N^{-1}'], 'best')
show()

