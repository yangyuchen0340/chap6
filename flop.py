import os
import numpy as np

#N=np.array([])
err_l=np.array([0.0211843,0.0113242,0.00587841,0.00299793,0.00151426,0.000761035])
err_g=np.array([0.00701228,0.00258955,0.00106507,0.000473974,0.000237044,0.000115186])
flops_g=[]
flops_l=[]
for i in range(1,7):
    modname_l='perf_flop_l%d'%i
    modname_g='perf_flop_g%d'%i
    options_g=[' -snes_type newtonls -snes_grid_sequence %d '%i,
               ' -da_refine 1 -ksp_rtol 1e-9 ',
               ' -pc_type gamg -mms 1 ',
               ' -snes_converged_reason','-log_view',':%s.py:ascii_info_detail'%modname_g]
    os.system('./bin/ex5 '+' '.join(options_g))
    perfmod_g=__import__(modname_g)
    flops_g.append(perfmod_g.LocalFlops[0])
    options_l=[' -snes_type newtonls -snes_grid_sequence %d '%i,
               ' -da_refine 1 -ksp_rtol 1e-9 ',
               ' -pc_type lu -mms 1 ',
               ' -snes_converged_reason','-log_view',':%s.py:ascii_info_detail'%modname_l]
    os.system('./bin/ex5 '+' '.join(options_l))
    perfmod_l=__import__(modname_l)
    flops_l.append(perfmod_l.LocalFlops[0])
print zip(flops_g,err_g,flops_l,err_l)

from pylab import legend, plot,loglog,show,title,xlabel,ylabel
loglog(flops_g,err_g,'b-',flops_l,err_l,'r-')
title('Work-precison graph MMS1 Part II')
legend(['GAMG','LU'],'best')
xlabel('Flops')
ylabel('l_2 error')
show() 
